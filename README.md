_This has only been tested on a machine running Ubuntu 16.04._
# ACA Installation

Once the dependencies listed below are installed on your machine, from the base directory of this repository, run `./install.sh`.

Try ACA out by running `./aca -d full -p cpaSeq test.c`. To see all options, run `./aca -h`.

# TO DOCUMENT
+ using 64-bit Linux builds for CVC4 and Z3

# Dependencies
#### Ubuntu Packages
+ `libc6-dev-i386`
+ `python-pycparser`

(You can install the above packages by running `sudo apt install <package>`.)
#### Programs

+ `java8`
+ `bash`
+ `python`
+ `benchexec`

#### Installing BenchExec

The following instructions are taken from the BenchExec installation instructions for Ubuntu; for other systems, please refer to the [installation documentation](https://github.com/sosy-lab/benchexec/blob/master/doc/INSTALL.md).

1. Download BenchExec's `.deb` [package file](https://github.com/sosy-lab/benchexec/releases/download/1.16/benchexec_1.16-1_all.deb).
2. Change directories to the folder containing the downloaded `.deb` file and run:
    + `sudo apt install python3-tempita`
    + `sudo dpkg -i benchexec_*.deb`
3. BenchExec also automatically configures the necessary cgroup permissions. Just add your user to the group `benchexec`:
    + `sudo adduser <USER> benchexec`
4. Reboot to effect the cgroup changes.
5. After installing BenchExec and setting up the cgroups file system, please run:
    + `python3 -m benchexec.check_cgroups`
    + If running the preceding command does not print out warnings, BenchExec should be successfully installed on your machine. This command will report warnings and exit with code 1 if something is missing. If you find that something does not work, please check the list of solutions found at the bottom of the [installation page](https://github.com/sosy-lab/benchexec/blob/master/doc/INSTALL.md). 