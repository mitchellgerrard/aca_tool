#!/bin/bash

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

C_FILE=$1
XML_FILE=$2
OUTPUT_DIR=$3
ANALYSIS_DIR=$4
TIMEOUT=$5

cd $ANALYSIS_DIR
benchexec $XML_FILE --container --overlay-dir / -o $OUTPUT_DIR -T $TIMEOUT
cd $CURRENT_DIR
