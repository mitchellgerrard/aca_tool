#!/bin/bash

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

CONFIG="$(echo $1 | tr '[:upper:]' '[:lower:]')"
INPUT_FILE=$(readlink -e "$2")
DIRECT_FILE=$(readlink -e "$3")

cd $CURRENT_DIR
cd ../civl

case "$CONFIG" in
	analyzer)
		java -jar civl.jar verify -svcomp17 "$INPUT_FILE"
		;;
	direct)
		timeout 120 java -jar civl.jar verify -svcomp16 -direct="$DIRECT_FILE" "$INPUT_FILE"
		;;
	slicer)
		timeout 120 java -ea -jar civl.jar replay -sliceAnalysis "$INPUT_FILE"
		;;
	*)
		echo "Unrecognized option: $1 (accepted values are 'analyzer','direct', 'slicer')"
		exit 1
		;;
esac


