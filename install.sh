#!/bin/bash

function check_for {
    command -v $1 >/dev/null 2>&1 || { echo >&2 "I require $1 but it's not installed.  Aborting."; exit 1; }
}

function does_package_exist {
    dpkg -l $1 >/dev/null 2>&1 || { echo >&2 "I cannot find $1 in your package manager; please install it. Aborting."; exit 1; }
}

check_for "java"
check_for "python"
check_for "benchexec"
does_package_exist "libc6-dev-i386"
does_package_exist "python-pycparser"

ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ANALYZERS="$ROOT/analyzers/"
SV_ARCHIVES="$ANALYZERS/svcomp18_archives/"
ACA_CONFIG="$HOME/.aca.config"

if [ -f $ACA_CONFIG ]
then
    echo
    echo " ! A file named $CONFIG_FILE already exists"
    echo " ! Renaming the existing '$ACA_CONFIG' to '$ACA_CONFIG.old'"
    echo
    mv $ACA_CONFIG $ACA_CONFIG.old
fi
echo "Writing ACA configuration to $ACA_CONFIG"
echo "$ANALYZERS" > $ACA_CONFIG
echo
echo "Configuring CIVL with the available solvers"
CIVL_DIR="$ANALYZERS/civl"
CIVL_JAR="$CIVL_DIR/civl.jar"
SOLVER_DIR="$CIVL_DIR/solvers"
cd $SOLVER_DIR
function unpack_and_rename_zip_if_needed {
    # first: canonical folder name (e.g., CPA_Seq)
    # second: zip file name (e.g., cpa-seq)
    # third: unpacked folder name (e.g., CPAchecker-1.6.23-svn\ 26773-unix)
    if [ ! -d "$1" ]; then
	echo "-> unpacking $1"
	if [[ $1 == "InterpChecker" ]]; then
	    unzip -q $2.zip
        else
	    unzip -q $2.zip && mv "$3" $1;
	fi
    fi
}
unpack_and_rename_zip_if_needed "z3" "z3-4.7.1-x64-ubuntu-16.04" "z3-4.7.1-x64-ubuntu-16.04"
# put our z3 at beginning of path for CIVL configuration
PATH=$SOLVER_DIR/z3/bin:$PATH
PATH=$SOLVER_DIR/cvc4/bin:$PATH
java -jar $CIVL_JAR config
cd $ROOT
echo
echo "Unpacking any zipped SVCOMP18 archives"
cd $SV_ARCHIVES
unpack_and_rename_zip_if_needed "CPA_Seq" "cpa-seq" "CPAchecker-1.6.23-svn 26773-unix"
unpack_and_rename_zip_if_needed "CPA_BAM_BnB" "cpa-bam-bnb" "cpa-bam-bnb"
unpack_and_rename_zip_if_needed "CPA_BAM_Slicing" "cpa-bam-slicing" "cpa-bam-slicing"
unpack_and_rename_zip_if_needed "InterpChecker" "interpchecker"
unpack_and_rename_zip_if_needed "UAutomizer" "uautomizer" "UAutomizer-linux"
unpack_and_rename_zip_if_needed "UKojak" "ukojak" "UKojak-linux"
unpack_and_rename_zip_if_needed "UTaipan" "utaipan" "UTaipan-linux"
unpack_and_rename_zip_if_needed "VeriAbs" "veriabs" "veriabs"
unpack_and_rename_zip_if_needed "CBMC" "cbmc" "cbmc" 
unpack_and_rename_zip_if_needed "TwoLS" "2ls" "2ls"
unpack_and_rename_zip_if_needed "DepthK" "depthk" "depthk-depthk_v3"
unpack_and_rename_zip_if_needed "ESBMC_incr" "esbmc-incr" "esbmc-v4.5"
unpack_and_rename_zip_if_needed "ESBMC_kind" "esbmc-kind" "esbmc-v4.5"
unpack_and_rename_zip_if_needed "Symbiotic" "symbiotic" "symbiotic"
cd $ROOT
echo
echo "To see options, run:"
echo
echo "  ./aca -h"
echo
echo "To test out ACA (with debugging on) on a single analyzer, run:"
echo
echo "  ./aca -d full -p cpaSeq test.c"
echo
